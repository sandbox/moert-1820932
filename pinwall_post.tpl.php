<li>
  <strong><?php print t('@username wrote on @date:', array('@username' => $username, '@date' => format_date($record->created, 'normal'))); ?></strong>
  <p><?php print $record->post; ?></p>
  <?php print $replies; ?>
  <?php print l(t('reply'), 'user/' . arg(1) . '/pinwall/reply/' . $record->id); ?>
</li>
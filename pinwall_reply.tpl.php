<li>
  <strong><?php print t('@username answered on @date:', array('@username' => $username, '@date' => format_date($record->created, 'normal'))); ?></strong>
  <p><?php print $record->reply; ?></p>
</li>